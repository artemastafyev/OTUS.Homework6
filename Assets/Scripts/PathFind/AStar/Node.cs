﻿using System;
using System.Collections.Generic;

namespace Algorithms.PathFind
{
    public class Node
    {
        public Node(int x, int y, string nodeType)
        {
            X = x;
            Y = y;
            NodeType = nodeType;
        }

        public List<Node> Related { get; } = new List<Node>();

        public float X { get; }

        public float Y { get; }

        public string NodeType { get; set; }

        public float Cost { get; set; }

        public Node Previous { get; set; }

        public void AddRelated(Node node)
        {
            if (NodeType == NodeTypes.Wall)
                return;

            if (node.NodeType != NodeTypes.Wall)
                Related.Add(node);
        }

        public float GetDistance(Node node)
        {
            return (float)Math.Sqrt(
                Math.Pow(node.X - X, 2) +
                Math.Pow(node.Y - Y, 2)
                );
        }
    }
}