﻿namespace Algorithms.PathFind
{
    public static class NodeTypes
    {
        public const string Wall = "X";
        public const string Space = "O";
        public const string StartPoint = "A";
        public const string EndPoint = "B";
        public const string Path = "P";
    }
}