﻿using Algorithms.PathFind;
using System.Text;

namespace Algorithms.PathFind
{
    public class GraphPrinter
    {
        public string PrintNodes(Graph graph)
        {
            Node[,] nodes = graph.Nodes;

            StringBuilder stringBuilder = new StringBuilder();

            for (int y = 0; y < nodes.GetLength(0); y++)
            {
                for (int x = 0; x < nodes.GetLength(1); x++)
                {
                    stringBuilder.Append(nodes[y, x].NodeType);
                }
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        public string PrintRelations(Graph graph)
        {
            Node[,] nodes = graph.Nodes;

            StringBuilder stringBuilder = new StringBuilder();

            for (int y = 0; y < nodes.GetLength(0); y++)
            {
                for (int x = 0; x < nodes.GetLength(1); x++)
                {
                    stringBuilder.Append(nodes[y, x].Related.Count);
                }
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }
    }
}
