﻿namespace Algorithms.PathFind
{
    public class GraphBuilder
    {
        public Graph Build(string map, int mapWidth, int mapHeight)
        {
            var nodes = CreateNodes(map, mapWidth, mapHeight);

            AddRelations(nodes);

            return new Graph(nodes);
        }

        private Node[,] CreateNodes(string map, int mapWidth, int mapHeight)
        {
            Node[,] nodes = new Node[mapHeight, mapWidth];

            for (int y = 0; y < mapHeight; y++)
                for (int x = 0; x < mapWidth; x++)
                    nodes[y, x] = new Node(x, y, map[y * mapWidth + x].ToString());
            return nodes;
        }

        private void AddRelations(Node[,] nodes)
        {
            int mapHeight = nodes.GetLength(0);
            int mapWidth = nodes.GetLength(1);

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    if (y - 1 >= 0)
                        nodes[y, x].AddRelated(nodes[y - 1, x]);

                    if (x - 1 >= 0)
                        nodes[y, x].AddRelated(nodes[y, x - 1]);

                    if (y + 1 < mapHeight)
                        nodes[y, x].AddRelated(nodes[y + 1, x]);

                    if (x + 1 < mapWidth)
                        nodes[y, x].AddRelated(nodes[y, x + 1]);
                }
            }
        }
    }
}
