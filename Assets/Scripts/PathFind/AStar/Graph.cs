﻿using System.Collections.Generic;

namespace Algorithms.PathFind
{
    public class Graph
    {
        public Node[,] Nodes { get; }

        public Graph(Node[,] nodes)
        {
            Nodes = nodes;
        }

        public void DrawPath(List<Node> path)
        {
            foreach (Node node in path)
            {
                if (node.NodeType != NodeTypes.StartPoint &&
                   node.NodeType != NodeTypes.EndPoint)
                {
                    node.NodeType = NodeTypes.Path;
                }
            }
        }
    }
}