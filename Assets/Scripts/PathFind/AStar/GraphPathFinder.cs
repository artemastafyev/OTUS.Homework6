﻿using Algorithms.PathFind;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithms.PathFind
{
    public class GraphPathFinder
    {
        public List<Node> FindPath(Graph graph)
        {
            var nodes = graph.Nodes.Flatten();

            var startNode = nodes.FirstOrDefault(x => x.NodeType == NodeTypes.StartPoint);
            if (startNode == null)
                throw new ArgumentException("Начальная точка пути не задана!");

            var endNode = nodes.FirstOrDefault(x => x.NodeType == NodeTypes.EndPoint);
            if (endNode == null)
                throw new ArgumentException("Конечная точка пути не задана!");

            SetInitialCosts(nodes, startNode);

            List<Node> reachable = new List<Node>() { startNode };
            List<Node> explored = new List<Node>();

            while (reachable.Count > 0)
            {
                var currentNode = ChooseNode(reachable, endNode);
                if (currentNode.NodeType == NodeTypes.EndPoint)
                {
                    return BuildPath(currentNode);
                }

                reachable.Remove(currentNode);
                explored.Add(currentNode);

                var newReachable = currentNode.Related.Except(explored).ToList();
                foreach (var relatedNode in newReachable)
                {
                    if (!reachable.Contains(relatedNode))
                    {
                        reachable.Add(relatedNode);
                    }

                    if (currentNode.Cost + 1 < relatedNode.Cost)
                    {
                        relatedNode.Previous = currentNode;
                        relatedNode.Cost = currentNode.Cost + 1;
                    }
                }
            }

            return new List<Node>();
        }

        private static void SetInitialCosts(IEnumerable<Node> nodes, Node startNode)
        {
            foreach (var node in nodes)
                node.Cost = float.PositiveInfinity;

            startNode.Cost = 0;
        }

        private static Node ChooseNode(List<Node> reachable, Node endNode)
        {
            float minCost = float.PositiveInfinity;
            Node bestNode = null;

            foreach (var node in reachable)
            {
                float costStartToNode = node.Cost;
                float costNodeToGoal = node.GetDistance(endNode);
                float totalCost = costStartToNode + costNodeToGoal;

                if (minCost > totalCost)
                {
                    minCost = totalCost;
                    bestNode = node;
                }
            }

            return bestNode;
        }

        private static List<Node> BuildPath(Node endNode)
        {
            List<Node> path = new List<Node>();

            Node currentNode = endNode;

            while (currentNode != null)
            {
                path.Add(currentNode);
                currentNode = currentNode.Previous;
            }

            return path;
        }
    }
}
