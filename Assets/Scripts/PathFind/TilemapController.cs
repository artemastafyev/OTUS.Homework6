using Algorithms.PathFind;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapController : MonoBehaviour
{
    [SerializeField] private Sprite wall;
    [SerializeField] private Sprite space;
    [SerializeField] private Sprite path;

    [SerializeField] private Tilemap tilemap;
    [SerializeField] private int wallCount = 40;
    
    private int _width = 18;
    private int _height = 10;

    // Start is called before the first frame update
    void Start()
    {
        GenerateMap();
    }

    public void GenerateMap()
    {
        FillSpace();

        SetStartPoint();

        SetEndPoint();

        GenerateRandomWalls();
    }

    public void FindPath()
    {
        string map = GenerateMapText();

        GraphBuilder graphBuilder = new GraphBuilder();
        Graph graph = graphBuilder.Build(map, _width, _height);

        GraphPathFinder pathFinder = new GraphPathFinder();
        List<Node> path = pathFinder.FindPath(graph);

        graph.DrawPath(path);

        for (int y = 0; y < _height; y++)
            for (int x = 0; x < _width; x++)
            {
                tilemap.SetTile(new Vector3Int(x, y, 0), GetTile(graph.Nodes[y, x].NodeType));
            }
    }

    private string GenerateMapText()
    {
        StringBuilder mapBuilder = new StringBuilder();

        for (int y = 0; y < _height; y++)
            for (int x = 0; x < _width; x++)
            {
                var tile = (GraphTile)tilemap.GetTile(new Vector3Int(x, y, 0));
                mapBuilder.Append(tile.nodeType);
            }

        return mapBuilder.ToString();
    }

    private void FillSpace()
    {
        for (int y = 0; y < _height; y++)
            for (int x = 0; x < _width; x++)
                tilemap.SetTile(new Vector3Int(x, y, 0), GetTile(NodeTypes.Space));
    }
    private void SetStartPoint()
    {
        int startPointX = Random.Range(0, _width / 2);
        int startPointY = Random.Range(0, _height / 2);
        tilemap.SetTile(new Vector3Int(startPointX, startPointY, 0), GetTile(NodeTypes.StartPoint));
    }

    private void SetEndPoint()
    {
        int endPointX = Random.Range(_width / 2, _width);
        int endPointY = Random.Range(_height / 2, _height);
        tilemap.SetTile(new Vector3Int(endPointX, endPointY, 0), GetTile(NodeTypes.EndPoint));
    }

    private void GenerateRandomWalls()
    {
        int counter = wallCount;
        int breakCounter = 0;

        while (counter > 0)
        {
            int x = Random.Range(0, _width);
            int y = Random.Range(0, _height);

            var position = new Vector3Int(x, y, 0);

            var tile = (GraphTile)tilemap.GetTile(position);
            if (tile.nodeType == NodeTypes.Space)
            {
                tilemap.SetTile(position, GetTile(NodeTypes.Wall));
                counter--;
            }

            breakCounter++;
            if (breakCounter > 1000)
                break;
        }
    }

    TileBase GetTile(string nodeType)
    {
        GraphTile tile = new GraphTile();
        tile.nodeType = nodeType;

        if (nodeType == NodeTypes.Space)
        {
            tile.sprite = space;
        }

        if (nodeType == NodeTypes.StartPoint)
        {
            tile.sprite = space;
            tile.color = Color.red;
        }

        if (nodeType == NodeTypes.EndPoint)
        {
            tile.sprite = space;
            tile.color = Color.green;
        }

        if (nodeType == NodeTypes.Wall)
        {
            tile.sprite = wall;
        }

        if (nodeType == NodeTypes.Path)
        {
            tile.sprite = path;
        }

        return tile;
    }
}