﻿using System;

namespace Algorithms.Sort
{
    public class Tree
    {
        public Tree left;
        public Tree right;
        public int key;

        public Tree(int k)
        {
            key = k;
        }

        public void Insert(Tree node)
        {
            if (node == null)
                throw new ArgumentNullException(nameof(node));

            if (node.key < key)
            {
                if (left == null) left = node;
                else left.Insert(node);
            }
            else
            {
                if (right == null) right = node;
                else right.Insert(node);
            }
        }

        public void Traverse(IBinaryTreeVisitor visitor)
        {
            if (visitor == null)
                throw new ArgumentNullException(nameof(visitor));

            if (left != null)
                left.Traverse(visitor);

            visitor.Visit(this);

            if (right != null)
                right.Traverse(visitor);
        }

        public void TraverseReverse(IBinaryTreeVisitor visitor)
        {
            if (visitor == null)
                throw new ArgumentNullException(nameof(visitor));

            if (right != null)
                right.TraverseReverse(visitor);

            visitor.Visit(this);

            if (left != null)
                left.TraverseReverse(visitor);
        }
    }
}
