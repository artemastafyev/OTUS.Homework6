﻿namespace Algorithms.Sort
{
    public interface IBinaryTreeVisitor
    {
        void Visit(Tree tree);
    }
}
