using Algorithms.Sort;
using UnityEngine;

public class SortController : MonoBehaviour
{
    [SerializeField] private SortDirection direction;
    [SerializeField] private int count = 25;
    [SerializeField] private int[] array = new int[25];

    private TreeSorter _treeSorter = new TreeSorter();

    private void Start()
    {
        array = new int[count];
        FillArray();
    }

    [ContextMenu("��������� ������")]
    public void FillArray()
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = Random.Range(-50, 50);
        }
    }

    [ContextMenu("����������� ������")]
    public void Sort()
    {
        array = _treeSorter.Sort(array, direction);
    }
}
