﻿using System;

namespace Algorithms.Sort
{
    public class TreeSorter
    {
        public int[] Sort(int[] array, SortDirection sortDirection = SortDirection.Ascending)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));

            if (array.Length == 0)
                return new int[0];

            var tree = new Tree(array[0]);

            for (int i = 1; i < array.Length; i++)
                tree.Insert(new Tree(array[i]));

            var visitor = new ResultCollectorVisitor(array);

            if (sortDirection == SortDirection.Ascending)
                tree.Traverse(visitor);
            else
                tree.TraverseReverse(visitor);

            return visitor.Result;
        }
    }
}
