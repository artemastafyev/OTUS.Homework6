﻿namespace Algorithms.Sort
{
    public enum SortDirection
    {
        Ascending,
        Descending
    }
}
