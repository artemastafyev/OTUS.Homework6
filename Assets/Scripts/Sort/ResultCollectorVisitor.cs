﻿using System;

namespace Algorithms.Sort
{
    public class ResultCollectorVisitor : IBinaryTreeVisitor
    {
        int[] sortedArray;
        int counter = 0;

        public ResultCollectorVisitor(int[] array)
        {
            sortedArray = new int[array.Length];
        }

        public void Visit(Tree tree)
        {
            if (tree == null)
                throw new ArgumentNullException(nameof(tree));

            sortedArray[counter++] = tree.key;
        }

        public int[] Result => sortedArray;
    }
}
