﻿using System;

namespace Algorithms.Sort
{
    public class PrintVisitor : IBinaryTreeVisitor
    {
        public void Visit(Tree tree)
        {
            if (tree == null)
                throw new ArgumentNullException(nameof(tree));

            Console.Write($"{tree.key} ");
        }
    }
}
